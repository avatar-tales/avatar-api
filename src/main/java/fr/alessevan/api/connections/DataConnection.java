package fr.alessevan.api.connections;

/**
 * Interface for the connection between the server and the client
 */
public interface DataConnection {

    /**
     * Connect to the server
     */
    void connect();

    /**
     * Disconnect from server
     */
    void disconnect();

}
