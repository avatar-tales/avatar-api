package fr.alessevan.api.connections.messaging;

import fr.alessevan.api.connections.exceptions.NoCodecFound;

import java.util.function.BiConsumer;

/**
 * Ability to send and receive object through RabbitMQ or internal methods
 */
public interface Messaging {

    /**
     * Register a new codec
     *
     * @param codec the codec to register
     * @param <T>   the type of the codec
     */
    <T> void addCodec(MessagingCodec<T> codec);

    /**
     * Send an object to the server
     *
     * @param object the object to send
     * @param target the target of the object
     * @param <T>    the type of the object
     * @throws NoCodecFound if no codec is found
     */
    <T> void send(T object, String target) throws NoCodecFound;

    /**
     * Return a default class codec
     *
     * @return the default codec
     */
    <T> MessagingCodec<T> getDefaultCodec(Class<T> clazz);

    /**
     * Codec to manipulate object
     *
     * @param <T> object to manipulate.
     */
    interface MessagingCodec<T> {

        /**
         * Encode an object in bytes array
         *
         * @param object the object to encode
         * @return the bytes array
         */
        byte[] encode(T object);

        /**
         * Decode an object from bytes array
         *
         * @param object the bytes array to decode
         * @return the object
         */
        T decode(byte[] object);

        /**
         * Send an object to messaging server
         *
         * @param object the object to send
         * @param target the target of the message
         */
        void send(T object, String target);

        /**
         * Receive an object from messaging server
         *
         * @param object the method to call when an object is received
         */
        void receive(BiConsumer<T, String> object);

        /**
         * Accept an object from messaging server
         *
         * @param object the object to accept
         * @param source the source of the object
         */
        void accept(T object, String source);

        /**
         * Reject an object from messaging server
         *
         * @param data   the byte array of the object to accept
         * @param source the source of the object
         */
        void accept(byte[] data, String source);

        /**
         * Get the consumer to execute when an object is received
         *
         * @return the consumer
         */
        BiConsumer<T, String> getConsumer();

        /**
         * Get the class of the codec's object
         *
         * @return the class
         */
        Class<T> getCodecClass();

    }
}
