package fr.alessevan.api.connections.messaging.rabbitmq;

import fr.alessevan.api.connections.DataConnection;
import fr.alessevan.api.connections.exceptions.NoCodecFound;
import fr.alessevan.api.connections.messaging.Messaging;

/**
 * Communicate with RabbitMQ server
 */
public interface RabbitMessaging extends DataConnection, Messaging {

    /**
     * Connect to the rabbitmq server
     */
    @Override
    void connect();

    /**
     * Add exchange key
     *
     * @param key the key to add
     */
    void addExchangeKey(String key);

    /**
     * Remove exchange key
     *
     * @param key the key to remove
     */
    void removeExchangeKey(String key);

    /**
     * Get codec from class object
     *
     * @param clazz the class of the codec's object
     * @param <T>   the object of the codec
     * @return the codec
     * @throws NoCodecFound if no codec is found
     */
    <T> RabbitCodec<T> getCodec(Class<T> clazz) throws NoCodecFound;

    /**
     * Send an object to the server
     *
     * @param object the object to send
     * @param key    the key to send the object
     * @param target the target of the object
     * @param <T>    the type of the object
     * @throws NoCodecFound if no codec is found
     */
    <T> void send(T object, String key, String target) throws NoCodecFound;

    /**
     * Disconnect from the server
     */
    @Override
    void disconnect();

    /**
     * Return a default class codec
     *
     * @return the default codec
     */
    <T> RabbitCodec<T> getDefaultRabbitCodec(Class<T> clazz);

    /**
     * Codec to manipulate object
     *
     * @param <T> object to manipulate.
     */
    interface RabbitCodec<T> extends MessagingCodec<T> {

        /**
         * Send an object to RabbitMQ server
         *
         * @param object the object to send
         * @param key    the key of the exchange
         * @param target the target of the message
         */
        void send(T object, String key, String target);

    }

}
