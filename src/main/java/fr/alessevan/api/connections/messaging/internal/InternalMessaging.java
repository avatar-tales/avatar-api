package fr.alessevan.api.connections.messaging.internal;

import fr.alessevan.api.connections.messaging.Messaging;

/**
 * Internal plugin messaging
 */
public interface InternalMessaging extends Messaging {
}
