package fr.alessevan.api.connections.exceptions;

/**
 * Exception thrown when no codec is found
 */
public class NoCodecFound extends Exception {

    /**
     * Constructor of the exception
     * @param clazz the class of the codec's object
     */
    public NoCodecFound(Class<?> clazz) {
        super("No codec found for the class " + clazz.getSimpleName());
    }

    /**
     * Constructor of the exception
     * @param className the name of the class of the codec's object
     */
    public NoCodecFound(String className) {
        super("No codec found for the class " + className);
    }
}
