package fr.alessevan.api.connections.sql.hikari;

import fr.alessevan.api.connections.sql.ConnectionFactory;

/**
 * Represents a Hikari connection
 */
public interface HikariConnection extends ConnectionFactory {


}
