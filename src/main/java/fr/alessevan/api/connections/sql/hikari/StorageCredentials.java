package fr.alessevan.api.connections.sql.hikari;

/**
 * Represents the credentials of a storage
 *
 * @param host     the host of the storage
 * @param port     the port of the storage
 * @param database the database of the storage
 * @param username the username of the storage
 * @param password the password of the storage
 */
public record StorageCredentials(String host, String port, String database, String username, String password) {
}
