package fr.alessevan.api.connections.sql;

import fr.alessevan.api.connections.DataConnection;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Represents a connection to a database
 */
public interface ConnectionFactory extends DataConnection {

    /**
     * Initialize the connection
     *
     * @param plugin the plugin
     */
    void init(JavaPlugin plugin);

    /**
     * Get the connection
     *
     * @return the connection
     * @throws SQLException if an error occurs
     */
    Connection getConnection() throws SQLException;

    /**
     * Get the type of the connection
     *
     * @return the type of the connection
     */
    String getConnectionType();

    /**
     * Get the keyword for auto increment
     *
     * @return the keyword for auto increment
     */
    String getAutoIncrement();

}
