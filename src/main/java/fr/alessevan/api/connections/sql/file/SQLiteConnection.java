package fr.alessevan.api.connections.sql.file;

import fr.alessevan.api.connections.sql.ConnectionFactory;

import java.nio.file.Path;

/**
 * Represents a SQLite connection
 */
public interface SQLiteConnection extends ConnectionFactory {

    /**
     * Get the path of the database file
     *
     * @return the path of the database file
     */
    Path getFilePath();

}
