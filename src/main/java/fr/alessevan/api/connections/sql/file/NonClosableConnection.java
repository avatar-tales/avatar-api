package fr.alessevan.api.connections.sql.file;

import java.sql.*;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * A {@link Connection} that cannot be closed.
 */
public class NonClosableConnection implements Connection {

    /**
     * The underlying connection.
     */
    private final Connection delegate;

    /**
     * Creates a new {@link NonClosableConnection} that delegates all calls to the given connection.
     *
     * @param delegate the connection to delegate to
     */
    public NonClosableConnection(Connection delegate) {
        this.delegate = delegate;
    }

    /**
     * Actually {@link #close() closes} the underlying connection.
     */
    public final void shutdown() throws SQLException {
        this.delegate.close();
    }

    /**
     * Does nothing.
     *
     * @throws SQLException exception
     */
    @Override
    public final void close() throws SQLException {
        // do nothing
    }

    /**
     * Returns the underlying connection.
     *
     * @param iface a Class defining an interface.
     * @return an object that implements the interface. Maybe a proxy for the actual implementing object.
     * @throws SQLException if no object found that implements the interface
     */
    @Override
    public final boolean isWrapperFor(Class<?> iface) throws SQLException {
        return iface.isInstance(this.delegate) || this.delegate.isWrapperFor(iface);
    }

    /**
     * @param iface A Class defining an interface that the result must implement.
     * @param <T>   type
     * @return an object that implements the interface. Maybe a proxy for the actual implementing object.
     * @throws SQLException exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public final <T> T unwrap(Class<T> iface) throws SQLException {
        if (iface.isInstance(this.delegate)) {
            return (T) this.delegate;
        }
        return this.delegate.unwrap(iface);
    }

    /**
     * @return the delegate connection
     * @throws SQLException exception
     */
    // Forward to the delegate connection
    @Override
    public Statement createStatement() throws SQLException {
        return this.delegate.createStatement();
    }

    /**
     * @param sql an SQL statement that may contain one or more '?' IN
     *            parameter placeholders
     * @return a new default {@code PreparedStatement} object containing the
     * @throws SQLException exception
     */
    @Override
    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return this.delegate.prepareStatement(sql);
    }

    /**
     * @param sql an SQL statement that may contain one or more '?'
     *            parameter placeholders. Typically, this statement is specified using JDBC
     *            call escape syntax.
     * @return a new default {@code CallableStatement} object containing the
     * @throws SQLException exception
     */
    @Override
    public CallableStatement prepareCall(String sql) throws SQLException {
        return this.delegate.prepareCall(sql);
    }

    /**
     * @param sql an SQL statement that may contain one or more '?'
     *            parameter placeholders
     * @return a new default {@code PreparedStatement} object, containing the
     * @throws SQLException exception
     */
    @Override
    public String nativeSQL(String sql) throws SQLException {
        return this.delegate.nativeSQL(sql);
    }

    /**
     * @return {@code true} if the connection has not been closed and is still valid
     * @throws SQLException exception
     */
    @Override
    public boolean getAutoCommit() throws SQLException {
        return this.delegate.getAutoCommit();
    }

    /**
     * @param autoCommit {@code true} to enable auto-commit mode;
     *                   {@code false} to disable it
     * @throws SQLException exception
     */
    @Override
    public void setAutoCommit(boolean autoCommit) throws SQLException {
        this.delegate.setAutoCommit(autoCommit);
    }

    /**
     * @throws SQLException exception
     */
    @Override
    public void commit() throws SQLException {
        this.delegate.commit();
    }

    /**
     * @throws SQLException exception
     */
    @Override
    public void rollback() throws SQLException {
        this.delegate.rollback();
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public boolean isClosed() throws SQLException {
        return this.delegate.isClosed();
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public DatabaseMetaData getMetaData() throws SQLException {
        return this.delegate.getMetaData();
    }

    /**
     * @throws SQLException
     */
    @Override
    public boolean isReadOnly() throws SQLException {
        return this.delegate.isReadOnly();
    }

    /**
     * @param readOnly {@code true} enables read-only mode;
     *                 {@code false} disables it
     * @throws SQLException
     */
    @Override
    public void setReadOnly(boolean readOnly) throws SQLException {
        this.delegate.setReadOnly(readOnly);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public String getCatalog() throws SQLException {
        return this.delegate.getCatalog();
    }

    /**
     * @param catalog the name of a catalog (subspace in this
     *                {@code Connection} object's database) in which to work
     * @throws SQLException
     */
    @Override
    public void setCatalog(String catalog) throws SQLException {
        this.delegate.setCatalog(catalog);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public int getTransactionIsolation() throws SQLException {
        return this.delegate.getTransactionIsolation();
    }

    /**
     * @param level one of the following {@code Connection} constants:
     *              {@code Connection.TRANSACTION_READ_UNCOMMITTED},
     *              {@code Connection.TRANSACTION_READ_COMMITTED},
     *              {@code Connection.TRANSACTION_REPEATABLE_READ}, or
     *              {@code Connection.TRANSACTION_SERIALIZABLE}.
     *              (Note that {@code Connection.TRANSACTION_NONE} cannot be used
     *              because it specifies that transactions are not supported.)
     * @throws SQLException
     */
    @Override
    public void setTransactionIsolation(int level) throws SQLException {
        this.delegate.setTransactionIsolation(level);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public SQLWarning getWarnings() throws SQLException {
        return this.delegate.getWarnings();
    }

    @Override
    public void clearWarnings() throws SQLException {
        this.delegate.clearWarnings();
    }

    /**
     * @param resultSetType        a result set type; one of
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency a concurrency type; one of
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @return
     * @throws SQLException
     */
    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
        return this.delegate.createStatement(resultSetType, resultSetConcurrency);
    }

    /**
     * @param sql                  a {@code String} object that is the SQL statement to
     *                             be sent to the database; may contain one or more '?' IN
     *                             parameters
     * @param resultSetType        a result set type; one of
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency a concurrency type; one of
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        return this.delegate.prepareStatement(sql, resultSetType, resultSetConcurrency);
    }

    /**
     * @param sql                  a {@code String} object that is the SQL statement to
     *                             be sent to the database; may contain on or more '?' parameters
     * @param resultSetType        a result set type; one of
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency a concurrency type; one of
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @return
     * @throws SQLException
     */
    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
        return this.delegate.prepareCall(sql, resultSetType, resultSetConcurrency);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public Map<String, Class<?>> getTypeMap() throws SQLException {
        return this.delegate.getTypeMap();
    }

    /**
     * @param map the {@code java.util.Map} object to install
     *            as the replacement for this {@code Connection}
     *            object's default type map
     * @throws SQLException
     */
    @Override
    public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
        this.delegate.setTypeMap(map);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public int getHoldability() throws SQLException {
        return this.delegate.getHoldability();
    }

    /**
     * @param holdability a {@code ResultSet} holdability constant; one of
     *                    {@code ResultSet.HOLD_CURSORS_OVER_COMMIT} or
     *                    {@code ResultSet.CLOSE_CURSORS_AT_COMMIT}
     * @throws SQLException
     */
    @Override
    public void setHoldability(int holdability) throws SQLException {
        this.delegate.setHoldability(holdability);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public Savepoint setSavepoint() throws SQLException {
        return this.delegate.setSavepoint();
    }

    /**
     * @param name a {@code String} containing the name of the savepoint
     * @return
     * @throws SQLException
     */
    @Override
    public Savepoint setSavepoint(String name) throws SQLException {
        return this.delegate.setSavepoint(name);
    }

    /**
     * @param savepoint the {@code Savepoint} object to roll back to
     * @throws SQLException
     */
    @Override
    public void rollback(Savepoint savepoint) throws SQLException {
        this.delegate.rollback(savepoint);
    }

    /**
     * @param savepoint the {@code Savepoint} object to be removed
     * @throws SQLException
     */
    @Override
    public void releaseSavepoint(Savepoint savepoint) throws SQLException {
        this.delegate.releaseSavepoint(savepoint);
    }

    /**
     * @param resultSetType        one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @param resultSetHoldability one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.HOLD_CURSORS_OVER_COMMIT} or
     *                             {@code ResultSet.CLOSE_CURSORS_AT_COMMIT}
     * @return
     * @throws SQLException
     */
    @Override
    public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return this.delegate.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    /**
     * @param sql                  a {@code String} object that is the SQL statement to
     *                             be sent to the database; may contain one or more '?' IN
     *                             parameters
     * @param resultSetType        one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @param resultSetHoldability one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.HOLD_CURSORS_OVER_COMMIT} or
     *                             {@code ResultSet.CLOSE_CURSORS_AT_COMMIT}
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return this.delegate.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    /**
     * @param sql                  a {@code String} object that is the SQL statement to
     *                             be sent to the database; may contain on or more '?' parameters
     * @param resultSetType        one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.TYPE_FORWARD_ONLY},
     *                             {@code ResultSet.TYPE_SCROLL_INSENSITIVE}, or
     *                             {@code ResultSet.TYPE_SCROLL_SENSITIVE}
     * @param resultSetConcurrency one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.CONCUR_READ_ONLY} or
     *                             {@code ResultSet.CONCUR_UPDATABLE}
     * @param resultSetHoldability one of the following {@code ResultSet}
     *                             constants:
     *                             {@code ResultSet.HOLD_CURSORS_OVER_COMMIT} or
     *                             {@code ResultSet.CLOSE_CURSORS_AT_COMMIT}
     * @return
     * @throws SQLException
     */
    @Override
    public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency, int resultSetHoldability) throws SQLException {
        return this.delegate.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
    }

    /**
     * @param sql               an SQL statement that may contain one or more '?' IN
     *                          parameter placeholders
     * @param autoGeneratedKeys a flag indicating whether auto-generated keys
     *                          should be returned; one of
     *                          {@code Statement.RETURN_GENERATED_KEYS} or
     *                          {@code Statement.NO_GENERATED_KEYS}
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
        return this.delegate.prepareStatement(sql, autoGeneratedKeys);
    }

    /**
     * @param sql           an SQL statement that may contain one or more '?' IN
     *                      parameter placeholders
     * @param columnIndexes an array of column indexes indicating the columns
     *                      that should be returned from the inserted row or rows
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
        return this.delegate.prepareStatement(sql, columnIndexes);
    }

    /**
     * @param sql         an SQL statement that may contain one or more '?' IN
     *                    parameter placeholders
     * @param columnNames an array of column names indicating the columns
     *                    that should be returned from the inserted row or rows
     * @return
     * @throws SQLException
     */
    @Override
    public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
        return this.delegate.prepareStatement(sql, columnNames);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public Clob createClob() throws SQLException {
        return this.delegate.createClob();
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public Blob createBlob() throws SQLException {
        return this.delegate.createBlob();
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public NClob createNClob() throws SQLException {
        return this.delegate.createNClob();
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public SQLXML createSQLXML() throws SQLException {
        return this.delegate.createSQLXML();
    }

    /**
     * @param timeout -             The time in seconds to wait for the database operation
     *                used to validate the connection to complete.  If
     *                the timeout period expires before the operation
     *                completes, this method returns false.  A value of
     *                0 indicates a timeout is not applied to the
     *                database operation.
     * @return
     * @throws SQLException
     */
    @Override
    public boolean isValid(int timeout) throws SQLException {
        return this.delegate.isValid(timeout);
    }

    /**
     * @param name  The name of the client info property to set
     * @param value The value to set the client info property to.  If the
     *              value is null, the current value of the specified
     *              property is cleared.
     * @throws SQLClientInfoException
     */
    @Override
    public void setClientInfo(String name, String value) throws SQLClientInfoException {
        this.delegate.setClientInfo(name, value);
    }

    /**
     * @param name The name of the client info property to retrieve
     * @return
     * @throws SQLException
     */
    @Override
    public String getClientInfo(String name) throws SQLException {
        return this.delegate.getClientInfo(name);
    }

    /**
     * @return
     * @throws SQLException
     */
    @Override
    public Properties getClientInfo() throws SQLException {
        return this.delegate.getClientInfo();
    }

    /**
     * @param properties the list of client info properties to set
     * @throws SQLClientInfoException
     */
    @Override
    public void setClientInfo(Properties properties) throws SQLClientInfoException {
        this.delegate.setClientInfo(properties);
    }

    /**
     * @param typeName the SQL name of the type the elements of the array map to. The typeName is a
     *                 database-specific name which may be the name of a built-in type, a user-defined type or a standard  SQL type supported by this database. This
     *                 is the value returned by {@code Array.getBaseTypeName}
     * @param elements the elements that populate the returned object
     * @return
     * @throws SQLException
     */
    @Override
    public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
        return this.delegate.createArrayOf(typeName, elements);
    }

    /**
     * @param typeName   the SQL type name of the SQL structured type that this {@code Struct}
     *                   object maps to. The typeName is the name of  a user-defined type that
     *                   has been defined for this database. It is the value returned by
     *                   {@code Struct.getSQLTypeName}.
     * @param attributes the attributes that populate the returned object
     * @return
     * @throws SQLException
     */
    @Override
    public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
        return this.delegate.createStruct(typeName, attributes);
    }

    /**
     * @return the current schema name or null
     * @throws SQLException
     */
    @Override
    public String getSchema() throws SQLException {
        return this.delegate.getSchema();
    }

    /**
     * @param schema the name of a schema  in which to work
     * @throws SQLException
     */
    @Override
    public void setSchema(String schema) throws SQLException {
        this.delegate.setSchema(schema);
    }

    /**
     * @param executor The {@code Executor}  implementation which will
     *                 be used by {@code abort}.
     * @throws SQLException
     */
    @Override
    public void abort(Executor executor) throws SQLException {
        this.delegate.abort(executor);
    }

    /**
     * @param executor     The {@code Executor}  implementation which will
     *                     be used by {@code setNetworkTimeout}.
     * @param milliseconds The time in milliseconds to wait for the database
     *                     operation
     *                     to complete.  If the JDBC driver does not support milliseconds, the
     *                     JDBC driver will round the value up to the nearest second.  If the
     *                     timeout period expires before the operation
     *                     completes, a SQLException will be thrown.
     *                     A value of 0 indicates that there is not timeout for database operations.
     * @throws SQLException
     */
    @Override
    public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
        this.delegate.setNetworkTimeout(executor, milliseconds);
    }

    /**
     * Get network timeout in milliseconds.
     *
     * @return network timeout in milliseconds
     * @throws SQLException if a database access error occurs
     */
    @Override
    public int getNetworkTimeout() throws SQLException {
        return this.delegate.getNetworkTimeout();
    }

}
