package fr.alessevan.api.connections.mongo;

import fr.alessevan.api.connections.DataConnection;
import fr.alessevan.api.connections.exceptions.NoCodecFound;

import java.util.List;

/**
 * Communicate with MongoDB server
 */
public interface MongoConnection extends DataConnection {

    /**
     * Connect to the mongodb server
     */
    @Override
    void connect();

    /**
     * Register a new codec
     *
     * @param codec the codec to register
     * @param <T>   the type of the codec
     */
    <T> void addCodec(MongoCodec<T> codec);

    <T> MongoCodec<T> getCodec(Class<T> clazz) throws NoCodecFound;

    /**
     * Disconnect from the server
     */
    @Override
    void disconnect();

    /**
     * Return a default class codec
     *
     * @return the default codec
     */
    <T> MongoCodec<T> getDefaultCodec(Class<T> clazz);

    /**
     * Codec to manipulate object
     *
     * @param <T> object to manipulate.
     */
    interface MongoCodec<T> {

        /**
         * Save an object to the database
         *
         * @param object the object to save
         */
        void save(T object);

        /**
         * Delete an object from the database
         *
         * @param object the object to delete
         * @return true if the object has been deleted
         */
        boolean delete(T object);

        /**
         * Update a value of an object
         *
         * @param id      the id of the object
         * @param var     the key of the value to update
         * @param content the new value
         * @param <V>     the type of the value
         */
        <V> void update(Object id, String var, V content);

        /**
         * Find an object by a key
         *
         * @param var       the key of the value to find
         * @param keyFilter the value to find
         * @param <V>       the type of the value
         * @return the object found
         */
        <V> T find(String var, V keyFilter);

        /**
         * Find an objects by sorting
         *
         * @param var the key of the value to sort with
         * @return the object found
         */
        List<T> findSorted(String var);

        /**
         * Find an objects by sorting and limiting
         *
         * @param var   the key of the value to sort with
         * @param limit the number of objects to return
         * @return the objects found
         */
        List<T> findSortedAndLimited(String var, int limit);

        /**
         * Find all objects
         *
         * @return list of all objects
         */
        List<T> findAll();

        /**
         * Get the class of the codec's type
         *
         * @return the class of the codec's type
         */
        Class<T> getCodecClass();

    }
}
