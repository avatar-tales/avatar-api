package fr.alessevan.api;

import fr.alessevan.api.commands.AvatarCommand;
import fr.alessevan.api.config.Config;
import fr.alessevan.api.connections.messaging.internal.InternalMessaging;
import fr.alessevan.api.connections.messaging.rabbitmq.RabbitMessaging;
import fr.alessevan.api.connections.mongo.MongoConnection;
import fr.alessevan.api.connections.sql.file.SQLiteConnection;
import fr.alessevan.api.connections.sql.hikari.HikariConnection;
import fr.alessevan.api.connections.sql.hikari.StorageCredentials;
import fr.alessevan.api.hooks.AvatarHooks;
import fr.alessevan.api.module.AvatarModule;
import fr.alessevan.api.utils.BossBar;
import fr.alessevan.api.utils.DisplayBar;
import net.kyori.adventure.text.Component;
import org.bukkit.NamespacedKey;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 * Abstract class of AvatarAPI
 *
 * @author Alessevan
 */
public abstract class AvatarAPI {

    /**
     * The API instance
     */
    private static AvatarAPI api;

    /**
     * Set the instance of AvatarAPI
     */
    public AvatarAPI() {
        api = this;
    }

    /**
     * Get the instance of AvatarAPI
     *
     * @return the instance of AvatarAPI
     */
    public static AvatarAPI getAPI() {
        return api;
    }

    /**
     * Get the core logger
     *
     * @return the core logger
     */
    public abstract Logger getLogger();

    /**
     * Retrieve Bukkit server instance
     *
     * @return the server instance
     */
    public abstract Server getServer();

    /**
     * Retrieve Core plugin instance
     *
     * @return the core plugin
     */
    public abstract JavaPlugin getPlugin();

    /**
     * Register a command
     *
     * @param plugin       the plugin attached to the command
     * @param commandClass the command class
     * @return the command instance
     */
    public abstract AvatarCommand registerCommand(JavaPlugin plugin, Class<? extends AvatarCommand> commandClass);

    /**
     * Register a command
     *
     * @param plugin  the plugin attached to the command
     * @param command the command instance
     */
    public abstract void registerCommand(JavaPlugin plugin, AvatarCommand command);

    /**
     * Unregister a command
     *
     * @param plugin       the plugin attached to the command
     * @param commandClass the command class
     */
    public abstract void unregisterCommand(JavaPlugin plugin, Class<? extends AvatarCommand> commandClass);

    /**
     * Unregister a command
     *
     * @param plugin  the plugin attached to the command
     * @param command the command
     */
    public abstract void unregisterCommand(JavaPlugin plugin, AvatarCommand command);

    /**
     * Unregister a command
     *
     * @param module  the module attached to the command
     * @param command the command
     * @param <T>     the type of the module
     */
    public <T extends JavaPlugin> void unregisterCommand(AvatarModule<T> module, AvatarCommand command) {
        try {
            unregisterCommand(module.plugin(), command);
            module.registeredCommands().remove(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Unregister module commands
     *
     * @param module the module
     * @param <T>    the type of the module
     */
    public <T extends JavaPlugin> void unregisterCommands(AvatarModule<T> module) {
        new ArrayList<>(module.registeredCommands()).forEach(command -> unregisterCommand(module.plugin(), command));
    }

    /**
     * Register a {@link AvatarHooks}
     *
     * @param hook the {@link AvatarHooks}
     */
    public abstract void registerServiceHook(AvatarHooks<?> hook);

    /**
     * Unregister a {@link AvatarHooks}
     *
     * @param hook the {@link AvatarHooks}
     */
    public abstract void unregisterServiceHook(AvatarHooks<?> hook);

    /**
     * Get a {@link AvatarHooks} by its service's class
     *
     * @param clazz the class of the {@link AvatarHooks}'s service
     * @param <T>   the service type
     * @return the {@link AvatarHooks}
     */
    public abstract <T> Optional<AvatarHooks<T>> getServiceHookByService(Class<T> clazz);

    /**
     * Get a {@link AvatarHooks} by its class
     *
     * @param clazz the class of the {@link AvatarHooks}
     * @param <T>   the {@link AvatarHooks} type
     * @return the {@link AvatarHooks}
     */
    public abstract <T extends AvatarHooks<?>> Optional<T> getServiceHook(Class<T> clazz);

    /**
     * Get all registered {@link AvatarHooks}
     *
     * @return all registered {@link AvatarHooks}
     */
    public abstract List<AvatarHooks<?>> getAllServiceHooks();

    /**
     * Register a {@link AvatarModule}
     *
     * @param plugin      the plugin attached to the {@link AvatarModule}
     * @param moduleClass the {@link AvatarModule} class
     * @param <T>         the plugin type
     */
    public abstract <T extends JavaPlugin> void registerModule(T plugin, Class<? extends AvatarModule<? extends JavaPlugin>> moduleClass);

    /**
     * Unregister a {@link AvatarModule} with its class
     *
     * @param moduleClass the {@link AvatarModule} class
     */
    public abstract void unregisterModule(Class<? extends AvatarModule<? extends JavaPlugin>> moduleClass);

    /**
     * Unregister a {@link AvatarModule} with its instance
     *
     * @param moduleClass the {@link AvatarModule} instance
     */
    public abstract void unregisterModule(AvatarModule<? extends JavaPlugin> moduleClass);

    /**
     * Register a pattern matching
     *
     * @param pattern  the pattern to match
     * @param function function to replace the matched string
     */
    public abstract void registerStringHook(Pattern pattern, Function<String, String> function);

    /**
     * Apply all registered hooks to a string
     *
     * @param sentence the string to apply hooks
     * @return the string with all hooks applied
     */
    public abstract String applyStringHooks(String sentence);

    /**
     * Apply all registered hooks to a string
     *
     * @param sentence the string to apply hooks
     * @return the component with all hooks applied
     */
    public abstract Component formatMessage(String sentence);

    /**
     * Create or get plugin's NamespacedKey
     *
     * @param plugin the plugin
     * @param key    the key
     * @return the NamespacedKey
     */
    public abstract NamespacedKey generateOrGetKey(JavaPlugin plugin, String key);

    /**
     * Create a blank {@link Config} file
     *
     * @param directory the directory of the {@link Config} file
     * @param fileName  the name of the {@link Config} file
     * @return the {@link Config} file
     */
    public abstract Config createConfig(Path directory, String fileName);

    /**
     * Create a {@link Config} file with a default {@link Config}
     *
     * @param directory    the directory of the {@link Config} file
     * @param fileName     the name of the {@link Config} file
     * @param resourcePath the path of the default {@link Config} file
     * @return the {@link Config} file
     */
    public abstract Config createConfig(Path directory, String fileName, String resourcePath);

    /**
     * Create a {@link Config} file with a default {@link Config}
     *
     * @param directory   the directory of the {@link Config} file
     * @param fileName    the name of the {@link Config} file
     * @param inputStream the path of the default {@link InputStream} of the file content
     * @return the {@link Config} file
     */
    public abstract Config createConfig(Path directory, String fileName, InputStream inputStream);

    /**
     * Get a {@link SQLiteConnection} instance to communicate with a SQLite database.
     *
     * @param plugin    plugin that want to create a database
     * @param directory the directory of the database file
     * @param fileName  the name of the database file
     * @return the {@link HikariConnection}
     */
    public abstract SQLiteConnection createSQLiteConnection(JavaPlugin plugin, Path directory, String fileName);

    /**
     * Get a {@link HikariConnection} instance to communicate with a MariaDB database.
     *
     * @param plugin      plugin that want to create a database
     * @param credentials the credentials to handle the connection
     * @return the {@link HikariConnection}
     */
    public abstract HikariConnection createMariaDBConnection(JavaPlugin plugin, StorageCredentials credentials);

    /**
     * Get a {@link HikariConnection} instance to communicate with a PostgreSQL database.
     *
     * @param plugin      plugin that want to create a database
     * @param credentials the credentials to handle the connection
     * @return the {@link HikariConnection}
     */
    public abstract HikariConnection createPostgreConnection(JavaPlugin plugin, StorageCredentials credentials);

    /**
     * Get a {@link MongoConnection} instance to communicate with a database
     *
     * @param host           host address
     * @param port           port
     * @param username       username
     * @param password       password
     * @param databaseName   database name
     * @param collectionName collection name
     * @return the {@link MongoConnection}
     */
    public abstract MongoConnection createMongoConnection(String host, int port, String username, String password, String databaseName, String collectionName);

    /**
     * Get a {@link RabbitMessaging} instance to communicate with a RabbitMQ server
     *
     * @param serverName server name (the plugin)
     * @param host       host address
     * @param port       port
     * @param username   username
     * @param password   password
     * @return the {@link RabbitMessaging}
     */
    public abstract RabbitMessaging createRabbitMQMessaging(String serverName, String host, int port, String username, String password);

    /**
     * Get a {@link InternalMessaging} instance to communicate with the plugin itself
     *
     * @return the {@link InternalMessaging}
     */
    public abstract InternalMessaging createInternalMessaging();

    public abstract void displayActionBar(DisplayBar bar, Player... players);

    public abstract void hideActionBar(DisplayBar bar, Player... players);

    public abstract void displayBossBar(BossBar bossBar, Player... players);

    public abstract void hideBossBar(BossBar bar, Player... players);

}
