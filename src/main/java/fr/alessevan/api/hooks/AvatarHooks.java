package fr.alessevan.api.hooks;

/**
 * Represents a hook
 *
 * @param <T> the type of the hooked object
 */
public interface AvatarHooks<T> {

    /**
     * Get the hooked object
     *
     * @return the hooked object
     */
    T getHooked();

}
