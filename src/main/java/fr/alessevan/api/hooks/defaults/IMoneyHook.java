package fr.alessevan.api.hooks.defaults;

import fr.alessevan.api.hooks.AvatarHooks;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a money hook
 *
 * @param <T> the money type
 */
public interface IMoneyHook<T> extends AvatarHooks<T> {

    /**
     * Get the money of a player
     *
     * @param player the player
     * @return the money
     */
    int getMoney(Player player);

    /**
     * Give money to a player
     *
     * @param player the player
     * @param amount the amount
     */
    void giveMoney(Player player, int amount);

    /**
     * Take money from a player
     *
     * @param player the player
     * @param amount the amount
     * @return true if the player has enough money
     */
    boolean takeMoney(Player player, int amount);

    /**
     * Register a money
     *
     * @param amount the amount
     * @param item   the item
     */
    void registerMoney(int amount, ItemStack item);

}
