package fr.alessevan.api.hooks.defaults;

import fr.alessevan.api.hooks.AvatarHooks;
import org.bukkit.Color;
import org.bukkit.inventory.ItemStack;

/**
 * Represents a rarity hook
 *
 * @param <T> the rarity type
 */
public interface IRarityHook<T> extends AvatarHooks<T> {

    /**
     * Get the rarity of an item
     *
     * @param item the item
     * @return the rarity
     */
    String getRarity(ItemStack item);

    /**
     * Get the colour of a rarity
     *
     * @param rarity the rarity
     */
    Color getBukkitColor(String rarity);

    /**
     * Set the colour of a rarity
     *
     * @param rarity the rarity
     * @param colour the colour in rgb
     */
    void setRarity(String rarity, int[] colour);

}
