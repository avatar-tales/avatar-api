package fr.alessevan.api.hooks.defaults;

import fr.alessevan.api.hooks.AvatarHooks;

import java.util.Map;
import java.util.UUID;

/**
 * Represents a LuckPerms hook
 *
 * @param <T> the LuckPerms type
 */
public interface ILuckPermsHook<T> extends AvatarHooks<T> {

    /**
     * Get the prefix of a player
     *
     * @param player the player
     * @return the prefix
     */
    String getPrefix(String player);

    /**
     * Get the prefix of a player
     *
     * @param uuid the uuid of the player
     * @return the prefix
     */
    String getPrefix(UUID uuid);

    /**
     * Get the suffix of a player
     *
     * @param player the player
     * @return the suffix
     */
    String getSuffix(String player);

    /**
     * Get the suffix of a player
     *
     * @param uuid the uuid of the player
     * @return the suffix
     */
    String getSuffix(UUID uuid);

    /**
     * Check if a player has a permission
     *
     * @param uuid       the uuid of the player
     * @param permission the permission
     * @return true if the player has the permission
     */
    boolean hasPermission(UUID uuid, String permission);

    /**
     * Add a permissions to a player
     *
     * @param uuid        the uuid of the player
     * @param permissions permissions
     */
    void addPermission(UUID uuid, String... permissions);

    /**
     * Add context permissions to a player
     *
     * @param uuid        the uuid of the player
     * @param context     the context
     * @param permissions permissions
     */
    void addPermission(UUID uuid, Map<String, String> context, String... permissions);

}
