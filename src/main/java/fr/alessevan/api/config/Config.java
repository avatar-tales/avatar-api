package fr.alessevan.api.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;

/**
 * The abstract class Config
 */
public abstract class Config {

    /**
     * The file of the config
     */
    protected final File fileConfig;
    protected boolean firstConfig;

    protected Config(final Path dataDirectory, final String fileName) {
        this.fileConfig = new File(dataDirectory.toFile(), fileName);
        this.firstConfig = false;
        try {
            if (!this.fileConfig.exists()) {
                this.fileConfig.getParentFile().mkdirs();
                this.fileConfig.createNewFile();
                this.firstConfig = true;
            }
            this.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected Config(final Path dataDirectory, final String fileName, final String resourcePath) {
        this(dataDirectory, fileName);
        if (this.isFirstConfig()) {
            try (final InputStream link = getClass().getResourceAsStream(resourcePath)) {
                if (link != null)
                    Files.copy(link, Path.of(dataDirectory.toFile().getAbsolutePath() + "/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                this.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    protected Config(final Path dataDirectory, final String fileName, final InputStream inputStream) {
        this(dataDirectory, fileName);
        if (this.isFirstConfig()) {
            try {
                Files.copy(inputStream, Path.of(dataDirectory.toFile().getAbsolutePath() + "/" + fileName), StandardCopyOption.REPLACE_EXISTING);
                this.load();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Load the config file
     *
     * @throws IOException exception
     */
    public abstract void load() throws IOException;

    /**
     * Replace the file content by a stream
     *
     * @param stream the stream
     * @throws IOException exception
     */
    public abstract void replaceFile(InputStream stream) throws IOException;

    /**
     * Add a default value to the config if not set
     *
     * @param path     the path
     * @param default_ the default value
     * @param <T>      the type of the value
     */
    public abstract <T> void addDefault(final String path, final T default_);

    /**
     * Get a value from the config, if not set, write the default value and return this value
     *
     * @param path     the path
     * @param default_ the default value
     * @param <T>      the type of the value
     * @return the value
     */
    public abstract <T> T getOrWrite(final String path, final T default_);

    /**
     * Get a value from the config or return default
     *
     * @param path     the path
     * @param default_ the default value
     * @param <T>      the type of the value
     * @return the value
     */
    public abstract <T> T get(final String path, final T default_);

    /**
     * Get a value from the config
     *
     * @param path the path
     * @return the list of subset keys
     */
    public abstract List<String> getKeys(final String path);

    /**
     * Write a value into the config
     *
     * @param path  the path
     * @param value the value
     * @param <T>   the type of the value
     */
    public abstract <T> void write(final String path, final T value);

    /**
     * Save the config
     *
     * @throws IOException exception
     */
    public abstract void save() throws IOException;

    /**
     * Get if the config is the first config
     *
     * @return if the config is the first config
     */
    public boolean isFirstConfig() {
        return this.firstConfig;
    }

    /**
     * Get the file of the config
     *
     * @return the file of the config
     */
    public File getFileConfig() {
        return this.fileConfig;
    }
}
