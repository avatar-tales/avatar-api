package fr.alessevan.api.items;

import com.destroystokyo.paper.Namespaced;
import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.common.collect.Multimap;
import fr.alessevan.api.AvatarAPI;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.minimessage.MiniMessage;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.*;

/**
 * Create item easily
 */
public class ItemBuilder {

    /**
     * The material of the item
     */
    private Material material;
    /**
     * The name of the item
     */
    private String name;
    /**
     * The amount of the item
     */
    private int amount;
    /**
     * The durability of the item
     */
    private int durability;
    /**
     * If the item is unbreakable
     */
    private boolean unbreakable;
    /**
     * The custom model data of the item
     */
    private int customModelData;
    /**
     * The lore of the item
     */
    private List<String> lore;
    /**
     * The flags of the item
     */
    private List<ItemFlag> flags;
    /**
     * The destroyable blocks of the item
     */
    private List<Namespaced> destroyableBlocks;
    /**
     * The enchantments of the item
     */
    private Map<String, Integer> enchantments;
    /**
     * The attributes of the item
     */
    private Map<Attribute, Collection<AttributeModifier>> attributes;
    /**
     * The owner of the item if it's a player head
     */
    private UUID owner;
    /**
     * The profile id of the item if it's a player head
     */
    private UUID profileId;
    /**
     * The profile textures of the item if it's a player head
     */
    private String profileTextures;

    /**
     * Create a new {@link ItemBuilder}
     */
    public ItemBuilder() {
        this.amount = 1;
        this.customModelData = -1;
        this.lore = new ArrayList<>();
        this.flags = new ArrayList<>();
        this.destroyableBlocks = new ArrayList<>();
        this.enchantments = new HashMap<>();
        this.attributes = new HashMap<>();
        this.profileId = UUID.randomUUID();
        this.profileTextures = "";
    }

    /**
     * Create an {@link ItemBuilder} from an {@link ItemStack}
     *
     * @param item The {@link ItemStack} to create the {@link ItemBuilder} from
     * @return The built {@link ItemStack}
     */
    public static ItemBuilder fromItemStack(ItemStack item) {
        ItemBuilder itemBuilder = new ItemBuilder()
                .material(item.getType())
                .flags(item.getItemFlags().stream().toList())
                .amount(item.getAmount());

        item.getEnchantments().forEach((enchantment, integer) ->
                itemBuilder.addEnchantment(enchantment.getKey().getNamespace() + ":" + enchantment.getKey().getKey(), integer)
        );

        List<Component> lore = item.lore();
        if (lore != null)
            itemBuilder.lore(lore.stream().map(MiniMessage.miniMessage()::serialize).toList());

        ItemMeta meta = item.getItemMeta();
        itemBuilder
                .unbreakable(meta.isUnbreakable())
                .destroyableBlocks(meta.getDestroyableKeys().stream().toList());

        if (meta.hasDisplayName())
            itemBuilder.name(MiniMessage.miniMessage().serialize(meta.displayName()));

        Multimap<Attribute, AttributeModifier> attributes = meta.getAttributeModifiers();
        if (attributes != null)
            itemBuilder.attributes(attributes.asMap());

        if (meta instanceof Damageable damageable)
            itemBuilder.durability(damageable.getDamage());

        if (meta.hasCustomModelData())
            itemBuilder.customModelData(meta.getCustomModelData());

        if (meta instanceof SkullMeta skullMeta) {
            if (skullMeta.hasOwner())
                itemBuilder.owner(skullMeta.getOwningPlayer().getUniqueId());
            PlayerProfile playerProfile = skullMeta.getPlayerProfile();
            if (playerProfile != null)
                itemBuilder.profile(playerProfile);
        }

        return itemBuilder;
    }

    /**
     * Create a new ItemBuilder
     *
     * @param material Material of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder material(Material material) {
        this.material = material;
        return this;
    }

    /**
     * Get the material of the item
     *
     * @return The material of the item
     */
    public Material material() {
        return this.material;
    }

    /**
     * Set the name of the item
     *
     * @param name Name of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder name(String name) {
        this.name = name;
        return this;
    }

    /**
     * Get the name of the item
     *
     * @return The name of the item
     */
    public String name() {
        return this.name;
    }

    /**
     * Set the amount of the item
     *
     * @param amount Amount of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder amount(int amount) {
        this.amount = Math.max(amount, 0);
        return this;
    }

    /**
     * Get the amount of the item
     *
     * @return The amount of the item
     */
    public int amount() {
        return this.amount;
    }

    /**
     * Set the durability of the item
     *
     * @param durability Durability of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder durability(int durability) {
        this.durability = durability;
        return this;
    }

    /**
     * Get the durability of the item
     *
     * @return The durability of the item
     */
    public int durability() {
        return this.durability;
    }

    /**
     * Set the unbreakable state of the item
     *
     * @param unbreakable Unbreakable state of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder unbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    /**
     * Get the unbreakable state of the item
     *
     * @return The unbreakable state of the item
     */
    public boolean unbreakable() {
        return this.unbreakable;
    }

    /**
     * Set the custom model data of the item
     *
     * @param customModelData Custom model data of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder customModelData(int customModelData) {
        this.customModelData = customModelData;
        return this;
    }

    /**
     * Get the custom model data of the item
     *
     * @return The custom model data of the item
     */
    public int customModelData() {
        return this.customModelData;
    }

    /**
     * Set the lore of the item
     *
     * @param lore Lore of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder lore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    /**
     * Add a line to the lore of the item
     *
     * @param line Line to add
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder addLore(String line) {
        this.lore.add(line);
        return this;
    }

    /**
     * Remove a line from the lore of the item
     *
     * @param line Line to remove
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder removeLore(int line) {
        this.lore.remove(line);
        return this;
    }

    /**
     * Get the lore of the item
     *
     * @return The lore of the item
     */
    public List<String> lore() {
        return this.lore;
    }

    /**
     * Set the flags of the item
     *
     * @param flags The flags of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder flags(List<ItemFlag> flags) {
        this.flags = flags;
        return this;
    }

    /**
     * Add a flag to the item
     *
     * @param flag The flag to add
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder addFlag(ItemFlag flag) {
        this.flags.add(flag);
        return this;
    }

    /**
     * Remove a flag from the item
     *
     * @param flag The flag to remove
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder removeFlag(ItemFlag flag) {
        this.flags.remove(flag);
        return this;
    }

    /**
     * Get the flags of the item
     *
     * @return The flags of the item
     */
    public List<ItemFlag> flags() {
        return this.flags;
    }

    /**
     * Set the destroyable blocks of the item
     *
     * @param destroyableBlocks The destroyable blocks of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder destroyableBlocks(List<Namespaced> destroyableBlocks) {
        this.destroyableBlocks = destroyableBlocks;
        return this;
    }

    /**
     * Add a block to the destroyable blocks of the item
     *
     * @param namespaced The block to add
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder addBlock(Namespaced namespaced) {
        this.destroyableBlocks.add(namespaced);
        return this;
    }

    /**
     * Remove a block from the destroyable blocks of the item
     *
     * @param namespaced The block to remove
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder removeBlock(Namespaced namespaced) {
        this.destroyableBlocks.remove(namespaced);
        return this;
    }

    /**
     * Get the destroyable blocks of the item
     *
     * @return The destroyable blocks of the item
     */
    public List<Namespaced> destroyableBlocks() {
        return this.destroyableBlocks;
    }

    /**
     * Set the enchantments of the item
     *
     * @param enchantments The enchantments of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder enchantments(Map<String, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    /**
     * Add an enchantment to the item
     *
     * @param enchantment The enchantment to add
     * @param level       The level of the enchantment
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder addEnchantment(String enchantment, Integer level) {
        this.enchantments.remove(enchantment);
        this.enchantments.put(enchantment, level);
        return this;
    }

    /**
     * Remove an enchantment from the item
     *
     * @param enchantment The enchantment to remove
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder removeEnchantment(NamespacedKey enchantment) {
        this.enchantments.remove(enchantment);
        return this;
    }

    /**
     * Get the enchantments of the item
     *
     * @return The enchantments of the item
     */
    public Map<String, Integer> enchantments() {
        return this.enchantments;
    }

    /**
     * Set the attributes of the item
     *
     * @param attributes The attributes of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder attributes(Map<Attribute, Collection<AttributeModifier>> attributes) {
        this.attributes = attributes;
        return this;
    }

    /**
     * Add an attribute to the item
     *
     * @param attribute The attribute to add
     * @param modifier  The modifier of the attribute
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder addAttribute(Attribute attribute, AttributeModifier modifier) {
        if (this.attributes.containsKey(attribute))
            this.attributes.get(attribute).add(modifier);
        else
            this.attributes.put(attribute, new ArrayList<>(List.of(modifier)));
        return this;
    }

    /**
     * Remove an attribute from the item
     *
     * @param attribute The attribute to remove
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder removeAttribute(Attribute attribute) {
        this.attributes.remove(attribute);
        return this;
    }

    public Map<Attribute, Collection<AttributeModifier>> attributes() {
        return this.attributes;
    }

    /**
     * Set the owner of the item FOR PLAYER_HEAD ONLY
     *
     * @param owner The owner of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder owner(UUID owner) {
        this.owner = owner;
        return this;
    }

    /**
     * Get the owner of the item
     *
     * @return The owner of the item
     */
    public UUID owner() {
        return this.owner;
    }

    /**
     * Set the profile of the item
     *
     * @param profile The profile of the item
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder profile(PlayerProfile profile) {
        this.profileId = profile.getId();
        if (profile.hasTextures())
            this.profileTextures = profile.getProperties().stream()
                    .filter(profileProperty -> profileProperty.getName().equals("textures"))
                    .map(ProfileProperty::getValue)
                    .findFirst()
                    .get();
        return this;
    }

    /**
     * Set the profile id of the item
     *
     * @param id The id of the profile
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder profile(UUID id) {
        this.profileId = id;
        return this;
    }

    /**
     * Set the profile textures of the item
     *
     * @param texture The textures of the profile
     * @return The {@link ItemBuilder}
     */
    public ItemBuilder profile(String texture) {
        this.profileTextures = texture;
        return this;
    }

    /**
     * Get the profile of the item
     *
     * @return The profile of the item
     */
    public PlayerProfile profile() {
        PlayerProfile profile = AvatarAPI.getAPI().getServer().createProfile(this.profileId, null);
        profile.getProperties().add(new ProfileProperty("textures", this.profileTextures));
        return profile;
    }

    /**
     * Build the item
     *
     * @return The built {@link ItemStack}
     */
    public ItemStack build() {
        if (this.material == null)
            this.material = Material.STONE;

        ItemStack item = new ItemStack(this.material, Math.min(this.amount, this.material.getMaxStackSize()));
        ItemMeta meta = item.getItemMeta();

        if (this.name != null)
            meta.displayName(MiniMessage.miniMessage().deserialize(this.name));
        if (meta instanceof Damageable damageable)
            damageable.setDamage(this.durability);

        if (!this.lore.isEmpty())
            meta.lore(this.lore.stream().map(MiniMessage.miniMessage()::deserialize).map(Component.class::cast).toList());
        if (!this.flags.isEmpty())
            meta.addItemFlags(this.flags.toArray(ItemFlag[]::new));
        if (!this.destroyableBlocks.isEmpty())
            meta.setDestroyableKeys(this.destroyableBlocks);
        if (this.customModelData > 0)
            meta.setCustomModelData(this.customModelData);

        this.enchantments.forEach((enchantment, level) -> meta.addEnchant(Enchantment.getByKey(new NamespacedKey(enchantment.split(":")[0], enchantment.split(":")[1])), level, true));
        this.attributes.keySet().forEach(attribute -> this.attributes.get(attribute).forEach(modifier -> meta.addAttributeModifier(attribute, modifier)));

        if (item.getType().equals(Material.PLAYER_HEAD)) {
            SkullMeta skullMeta = (SkullMeta) meta;
            if (this.owner != null)
                skullMeta.setOwningPlayer(Bukkit.getOfflinePlayer(this.owner));
            if (this.profileId != null && !this.profileTextures.isBlank())
                skullMeta.setPlayerProfile(this.profile());
        }

        item.setItemMeta(meta);
        return item;
    }

}
