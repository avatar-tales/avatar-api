package fr.alessevan.api.module;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * The annotation Parameter
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Parameter {

    /**
     * Path of the parameter in settings file.
     * If empty, it will be the name of the field.
     */
    String path() default "";

}
