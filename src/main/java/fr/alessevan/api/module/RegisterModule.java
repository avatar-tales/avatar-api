package fr.alessevan.api.module;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * To register module information
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RegisterModule {
    /**
     * Module name
     */
    String name();

    /**
     * Module version
     */
    String version();

    /**
     * Module author
     */
    String author() default "Unknown";

    /**
     * Module description
     */
    String description() default "";
}
