package fr.alessevan.api.module;

import fr.alessevan.api.AvatarAPI;
import fr.alessevan.api.commands.AvatarCommand;
import fr.alessevan.api.config.Config;
import org.bukkit.Server;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * The abstract class AvatarModule
 */
public abstract class AvatarModule<T extends JavaPlugin> {

    /**
     * The data folder to store data
     */
    protected final Path dataFolder;
    /**
     * The config of the module
     */
    protected final Config config;
    /**
     * The plugin
     */
    private final T plugin;
    /**
     * The {@link AvatarAPI}
     */
    private final AvatarAPI api;
    /**
     * The list of registered commands
     */
    private final List<AvatarCommand> registeredCommands;
    /**
     * If the module is enabled
     */
    @Parameter
    protected boolean enabled = false;

    /**
     * Constructor of AvatarModule
     *
     * @param plugin     The plugin
     * @param api        The {@link AvatarAPI}
     * @param dataFolder The data folder
     */
    public AvatarModule(T plugin, AvatarAPI api, Path dataFolder) {
        this.plugin = plugin;
        this.api = api;
        this.dataFolder = dataFolder;
        this.registeredCommands = new ArrayList<>();
        this.config = this.api.createConfig(this.dataFolder, "config.yml");
    }

    /**
     * Constructor of AvatarModule
     *
     * @param plugin     The plugin
     * @param api        The {@link AvatarAPI}
     * @param dataFolder The data folder
     * @param configName The name of the config file in the module's resources
     */
    public AvatarModule(T plugin, AvatarAPI api, Path dataFolder, String configName) {
        this.plugin = plugin;
        this.api = api;
        this.dataFolder = dataFolder;
        this.registeredCommands = new ArrayList<>();
        this.config = this.api.createConfig(this.dataFolder, "config.yml", this.plugin().getResource(configName));
    }

    /**
     * Get the {@link AvatarAPI}
     *
     * @return The {@link AvatarAPI}
     */
    public static AvatarAPI getAPI() {
        return AvatarAPI.getAPI();
    }

    /**
     * Called when the module is enabled
     */
    public abstract void onEnable();

    /**
     * Register events
     *
     * @param listeners The listeners
     */
    protected void registerEvents(Listener... listeners) {
        for (Listener listener : listeners) {
            this.plugin.getServer().getPluginManager().registerEvents(listener, this.plugin);
        }
    }

    /**
     * Register commands
     */
    protected void registerCommands(AvatarCommand... commands) {
        for (AvatarCommand command : commands) {
            this.api.registerCommand(this.plugin, command);
            this.registeredCommands.add(command);
        }
    }

    /**
     * Register commands from their class
     */
    protected final void registerCommands(Class<AvatarCommand>[] commands) {
        for (Class<AvatarCommand> command : commands) {
            this.registeredCommands.add(this.api.registerCommand(this.plugin, command));
        }
    }

    /**
     * Get server's instance
     */
    public Server getServer() {
        return this.plugin().getServer();
    }

    /**
     * Called when the module is disabled
     */
    public abstract void onDisable();

    /**
     * Check if the module is enabled
     *
     * @return True if the module is enabled
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * Get the plugin
     *
     * @return The plugin
     */
    public T plugin() {
        return this.plugin;
    }

    /**
     * Get the {@link AvatarAPI}
     *
     * @return The {@link AvatarAPI}
     */
    public AvatarAPI api() {
        return this.api;
    }

    /**
     * Get the {@link Config} of the module
     *
     * @return The {@link Config} of the module
     */
    public Config getConfig() {
        return this.config;
    }

    /**
     * Get the {@link Config} of the module
     *
     * @return The {@link Config} of the module
     */
    public Config config() {
        return this.config;
    }

    /**
     * Get the data folder of the module
     *
     * @return The data folder of the module
     */
    public Path dataFolder() {
        return this.dataFolder;
    }

    /**
     * Get the plugin logger
     *
     * @return the plugin logger
     */
    public Logger logger() {
        return this.plugin.getLogger();
    }

    /**
     * Get the registered commands
     *
     * @return The registered commands
     */
    public List<AvatarCommand> registeredCommands() {
        return this.registeredCommands;
    }

    /**
     * Get the name of the module
     *
     * @return The name of the module
     */
    public String getName() {
        return this.getClass().getAnnotation(RegisterModule.class).name();
    }

    /**
     * Get the version of the module
     *
     * @return The version of the module
     */
    public String getVersion() {
        return this.getClass().getAnnotation(RegisterModule.class).version();
    }

    /**
     * Get the author of the module
     *
     * @return The author of the module
     */
    public String getAuthor() {
        return this.getClass().getAnnotation(RegisterModule.class).author();
    }

    /**
     * Get the description of the module
     *
     * @return The description of the module
     */
    public String getDescription() {
        return this.getClass().getAnnotation(RegisterModule.class).description();
    }
}
