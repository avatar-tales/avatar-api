package fr.alessevan.api.inventory;

import fr.alessevan.api.AvatarAPI;
import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.apache.logging.log4j.util.TriConsumer;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class GUIItem {

    private ItemStack                                        item;
    private int                                              slot;
    private TriConsumer<GUI, ItemStack, InventoryClickEvent> onClick;

    private GUIItem() {
    }

    public ItemStack item() {
        return this.item;
    }

    public int slot() {
        return this.slot;
    }

    public TriConsumer<GUI, ItemStack, InventoryClickEvent> onClick() {
        return this.onClick;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private       ItemStack                itemStack;
        private       Material                 material;
        private       int                      amount;
        private       String                   name;
        private       List<String>             lore;
        private final List<Consumer<ItemMeta>> meta;

        private GUIItem guiItem;

        private Builder() {
            this.guiItem = new GUIItem();
            this.amount = 1;
            this.lore = new ArrayList<>();
            this.meta = new ArrayList<>();
        }

        public Builder itemStack(ItemStack item) {
            this.itemStack = item;
            return this;
        }

        public Builder material(Material material) {
            this.material = material;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder lore(@NotNull List<String> lore) {
            this.lore = lore;
            return this;
        }

        public Builder meta(Consumer<ItemMeta> meta) {
            this.meta.add(meta);
            return this;
        }


        public Builder slot(int slot) {
            this.guiItem.slot = slot;
            return this;
        }

        public Builder onClick(TriConsumer<GUI, ItemStack, InventoryClickEvent> onClick) {
            this.guiItem.onClick = onClick;
            return this;
        }

        public GUIItem build() {
            this.guiItem.item = Objects.requireNonNullElseGet(this.itemStack, () -> new ItemStack(this.material, this.amount));
            if (!this.guiItem.item.getType().name().contains("AIR")) {
                var baseLore = this.guiItem.item.lore();
                if (baseLore == null)
                    baseLore = new ArrayList<>();
                baseLore.addAll(this.lore.stream()
                        .map(line -> AvatarAPI.getAPI().formatMessage(line))
                        .toList());
                this.guiItem.item.lore(baseLore);
                if (this.itemStack == null)
                    this.guiItem.item.editMeta(meta -> meta.displayName(AvatarAPI.getAPI().formatMessage(this.name == null ? this.guiItem.item.getType().translationKey() : this.name)));
                this.meta.forEach(meta -> this.guiItem.item.editMeta(meta));
            }
            return this.guiItem;
        }


    }

}
