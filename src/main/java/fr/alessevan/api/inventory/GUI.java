package fr.alessevan.api.inventory;

import fr.alessevan.api.AvatarAPI;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerAttemptPickupItemEvent;
import org.bukkit.event.server.PluginDisableEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class GUI implements Listener, InventoryHolder {

    private JavaPlugin plugin;
    private Inventory inventory;
    private final List<GUIItem> items;
    private final List<Supplier<GUIItem>> itemsToRefresh;
    private final List<GUIItem> refreshed;
    private Supplier<List<GUIItem>> largeItemsToRefresh;
    private List<GUIItem> largeItemsRefreshed;
    private Consumer<InventoryOpenEvent> onOpen;
    private Consumer<InventoryClickEvent> onClick;
    private BiConsumer<GUI, InventoryCloseEvent> onClose;
    private int refresh;
    private boolean canPickup;
    private BukkitTask updater;

    private GUI() {
        this.items = new ArrayList<>();
        this.itemsToRefresh = new ArrayList<>();
        this.refreshed = new ArrayList<>();
        this.refresh = 0;
    }

    public void open(Player player) {
        player.openInventory(this.inventory);
    }

    @EventHandler
    public void onOpen(InventoryOpenEvent event) {
        if (!this.inventory.equals(event.getInventory()))
            return;
        if (this.onOpen != null)
            this.onOpen.accept(event);
    }

    @EventHandler
    public void onClick(InventoryClickEvent event) {
        if (!this.inventory.equals(event.getView().getTopInventory()))
            return;
        boolean wasCancelled = event.isCancelled();
        event.setCancelled(true);
        if (this.onClick != null)
            this.onClick.accept(event);

        if (!event.getView().getBottomInventory().equals(event.getClickedInventory())) {
            for (GUIItem guiItem : this.largeItemsRefreshed) {
                if (guiItem.slot() == event.getSlot() && guiItem.onClick() != null) {
                    guiItem.onClick().accept(this, guiItem.item(), event);
                    if (!wasCancelled && !event.isCancelled()) {
                        event.setCancelled(false);
                    }
                    return;
                }
            }
            for (GUIItem guiItem : this.refreshed) {
                if (guiItem.slot() == event.getSlot() && guiItem.onClick() != null) {
                    guiItem.onClick().accept(this, guiItem.item(), event);
                    if (!wasCancelled && !event.isCancelled()) {
                        event.setCancelled(false);
                    }
                    return;
                }
            }
            for (GUIItem guiItem : this.items) {
                if (guiItem.slot() == event.getSlot() && guiItem.onClick() != null) {
                    guiItem.onClick().accept(this, guiItem.item(), event);
                    if (!wasCancelled && !event.isCancelled()) {
                        event.setCancelled(false);
                    }
                    return;
                }
            }
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPickup(PlayerAttemptPickupItemEvent event) {
        if (!this.inventory.equals(event.getPlayer().getOpenInventory().getTopInventory()))
            return;
        event.setCancelled(!this.canPickup);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent event) {
        if (this.inventory.equals(event.getInventory())) {
            if (this.onClose != null)
                this.onClose.accept(this, event);
            if (this.updater != null)
                this.updater.cancel();
            this.plugin.getServer().getScheduler().runTaskLater(this.plugin, () ->
                            HandlerList.unregisterAll(this)
                    , 1L);
        }
    }

    @EventHandler
    public void onPluginDisable(PluginDisableEvent event) {
        if (this.plugin.equals(event.getPlugin())) {
            if (this.updater != null)
                this.updater.cancel();
            this.plugin.getServer().getOnlinePlayers().stream()
                    .filter(p -> this.equals(p.getOpenInventory().getTopInventory().getHolder()))
                    .forEach(Player::closeInventory);
        }
    }

    public void update() {
        this.refreshed.forEach(item -> this.inventory.setItem(item.slot(), new ItemStack(Material.AIR)));
        this.refreshed.clear();
        this.itemsToRefresh.stream().map(Supplier::get).forEach(item -> {
            this.refreshed.add(item);
            this.inventory.setItem(item.slot(), item.item());
        });
        this.largeItemsRefreshed.forEach(item -> this.inventory.setItem(item.slot(), new ItemStack(Material.AIR)));
        if (this.largeItemsToRefresh != null)
            this.largeItemsRefreshed = this.largeItemsToRefresh.get();
        this.largeItemsRefreshed.forEach(item -> {
            this.refreshed.add(item);
            this.inventory.setItem(item.slot(), item.item());
        });
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public @NotNull Inventory getInventory() {
        return this.inventory;
    }

    public static class Builder {

        private InventoryType type;
        private int size;
        private String title;
        private boolean canPickup = true;

        private final GUI gui;

        private Builder() {
            this.type = InventoryType.CHEST;
            this.size = 27;
            this.title = null;
            this.gui = new GUI();
        }

        /**
         * Change the inventory type
         *
         * @param type the new inventory type
         * @return the builder
         */
        public Builder type(InventoryType type) {
            this.type = type;
            return this;
        }

        /**
         * Change the inventory size
         *
         * @param size the new inventory size
         * @return the builder
         */
        public Builder size(int size) {
            this.size = size;
            return this;
        }

        /**
         * Change the inventory title
         *
         * @param title the new inventory title
         * @return the builder
         */
        public Builder title(String title) {
            this.title = title;
            return this;
        }

        /**
         * Allow or disallow the player to pickup items when the gui is opened
         *
         * @param canPickup the pickup state
         * @return the builder
         */
        public Builder pickup(boolean canPickup) {
            this.canPickup = canPickup;
            return this;
        }

        /**
         * Change refresh rate
         *
         * @param time the new refresh rate
         * @return the builder
         */
        public Builder refresh(int time) {
            this.gui.refresh = time;
            return this;
        }

        /**
         * Add an {@link GUIItem}to the inventory
         *
         * @param item the {@link GUIItem} to add
         * @return the builder
         */
        public Builder item(GUIItem item) {
            this.gui.items.add(item);
            return this;
        }

        /**
         * Add an {@link GUIItem} to the inventory.
         * It will create the item when the inventory is opened
         * and at each refresh
         *
         * @param item the item builder to add
         * @return the builder
         */
        public Builder item(Supplier<GUIItem> item) {
            this.gui.itemsToRefresh.add(item);
            return this;
        }

        /**
         * Add a large {@link GUIItem} to the inventory
         *
         * @param items the large {@link GUIItem} to add
         * @return the builder
         */
        public Builder allItems(Supplier<List<GUIItem>> items) {
            this.gui.largeItemsToRefresh = items;
            return this;
        }

        /**
         * Method to call when the inventory is opened
         *
         * @param onOpen the method to call
         * @return the builder
         */
        public Builder onOpen(Consumer<InventoryOpenEvent> onOpen) {
            this.gui.onOpen = onOpen;
            return this;
        }

        /**
         * Method to call when the inventory is interacted
         *
         * @param onClick the method to call
         * @return the builder
         */
        public Builder onClick(Consumer<InventoryClickEvent> onClick) {
            this.gui.onClick = onClick;
            return this;
        }

        /**
         * Method to call when the inventory is closed
         *
         * @param onClose the method to call
         * @return the builder
         */
        public Builder onClose(Consumer<InventoryCloseEvent> onClose) {
            this.gui.onClose = (gui, event) -> onClose.accept(event);
            return this;
        }

        /**
         * Method to call when the inventory is closed
         *
         * @param onClose the method to call
         * @return the builder
         */
        public Builder onClose(BiConsumer<GUI, InventoryCloseEvent> onClose) {
            this.gui.onClose = onClose;
            return this;
        }

        /**
         * Set the plugin's instance to register the events
         *
         * @param plugin the plugin's instance
         * @return the builder
         */
        public Builder plugin(JavaPlugin plugin) {
            this.gui.plugin = plugin;
            return this;
        }

        /**
         * Build the {@link GUI}
         *
         * @return the {@link GUI}
         */
        public GUI build() {
            if (this.type == InventoryType.CHEST)
                this.gui.inventory = this.gui.plugin.getServer().createInventory(this.gui, this.size, this.title == null ? this.type.defaultTitle() : AvatarAPI.getAPI().formatMessage(this.title));
            else
                this.gui.inventory = this.gui.plugin.getServer().createInventory(this.gui, this.type, this.title == null ? this.type.defaultTitle() : AvatarAPI.getAPI().formatMessage(this.title));

            this.gui.canPickup = this.canPickup;
            this.gui.items.forEach(item -> this.gui.inventory.setItem(item.slot(), item.item()));
            this.gui.itemsToRefresh.stream().map(Supplier::get).forEach(item -> {
                this.gui.refreshed.add(item);
                this.gui.inventory.setItem(item.slot(), item.item());
            });
            if (this.gui.largeItemsToRefresh != null)
                this.gui.largeItemsRefreshed = this.gui.largeItemsToRefresh.get();
            else
                this.gui.largeItemsRefreshed = new ArrayList<>();
            this.gui.largeItemsRefreshed.forEach(item -> {
                this.gui.refreshed.add(item);
                this.gui.inventory.setItem(item.slot(), item.item());
            });
            if ((!this.gui.itemsToRefresh.isEmpty() || this.gui.largeItemsToRefresh != null) && this.gui.refresh > 0) {
                this.gui.updater = this.gui.plugin.getServer().getScheduler().runTaskTimerAsynchronously(this.gui.plugin, this.gui::update, 0, this.gui.refresh);
            }

            this.gui.plugin.getServer().getPluginManager().registerEvents(this.gui, this.gui.plugin);
            return this.gui;
        }

    }

}
