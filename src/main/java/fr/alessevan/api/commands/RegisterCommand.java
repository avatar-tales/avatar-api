package fr.alessevan.api.commands;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * To register command information
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface RegisterCommand {

    /**
     * The name of the command
     *
     * @return the name of the command
     */
    String name();

    /**
     * The permission of the command
     *
     * @return the permission of the command
     */
    String permission() default "";

    /**
     * The usage of the command
     *
     * @return the usage of the command
     */
    String usage() default "";

    /**
     * The description of the command
     *
     * @return the description of the command
     */
    String description() default "";

    /**
     * The aliases of the command
     *
     * @return the aliases of the command
     */
    String[] aliases() default {};

    /**
     * The minimum number of arguments
     *
     * @return the minimum number of arguments
     */
    int minArgs() default 0;

    /**
     * The maximum number of arguments
     *
     * @return the maximum number of arguments
     */
    int maxArgs() default 99;

    /**
     * Choose who can execute the command
     *
     * @return the sender type of the command
     */
    Sender sender() default Sender.ALL;

    /**
     * The sender type of the command
     */
    enum Sender {
        /**
         * The command can be executed by a player
         */
        PLAYER,

        /**
         * The command can be executed by the console
         */
        CONSOLE,

        /**
         * The command can be executed by a player or the console (Default setting)
         */
        ALL
    }

}
