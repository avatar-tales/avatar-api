package fr.alessevan.api.commands;

import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * Interface of AvatarCommand
 * @author Alessevan
 */
public interface AvatarCommand {

    /**
     * Execute the command
     * @param sender the sender of the command
     * @param commandLabel the command label
     * @param args the command arguments
     * @return true if the command is executed
     */
    boolean execute(CommandSender sender, String commandLabel, List<String> args);

    /**
     * Execute the command
     * @param player the player of the command
     * @param commandLabel the command label
     * @param args the command arguments
     * @return true if the command is executed
     */
    default boolean execute(Player player, String commandLabel, List<String> args) {
        return execute((CommandSender) player, commandLabel, args);
    }

    /**
     * Execute the command
     * @param sender the console of the command
     * @param commandLabel the command label
     * @param args the command arguments
     * @return true if the command is executed
     */
    default boolean execute(ConsoleCommandSender sender, String commandLabel, List<String> args) {
        return execute((CommandSender) sender, commandLabel, args);
    }


    /**
     * Get completions for a command
     * @param sender the sender of the command
     * @param commandLabel the command label
     * @param args the command arguments
     * @return list of completions
     */
    default List<String> suggest(CommandSender sender, String commandLabel, List<String> args) {
        return null;
    }

}
