package fr.alessevan.api.utils;

import java.time.Duration;
import java.util.List;
import java.util.UUID;

public class BossBar extends DisplayBar {

    private net.kyori.adventure.bossbar.BossBar.Color color;
    private net.kyori.adventure.bossbar.BossBar.Overlay style;
    private List<net.kyori.adventure.bossbar.BossBar.Flag> flags;

    public BossBar(String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(message);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }

    public BossBar(String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, boolean permanent, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(message, permanent);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }

    public BossBar(String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, boolean permanent, int priority, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(message, permanent, priority);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }

    public BossBar(String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, boolean permanent, int priority, boolean visible, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(message, permanent, priority, visible);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }

    public BossBar(String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, boolean permanent, int priority, boolean visible, Duration duration, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(message, permanent, priority, visible, duration);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }

    public BossBar(UUID id, String message, net.kyori.adventure.bossbar.BossBar.Color color, net.kyori.adventure.bossbar.BossBar.Overlay style, boolean permanent, int priority, boolean visible, Duration duration, net.kyori.adventure.bossbar.BossBar.Flag... flags) {
        super(id, message, permanent, priority, visible, duration);
        this.color = color;
        this.style = style;
        this.flags = List.of(flags);
    }


    public net.kyori.adventure.bossbar.BossBar.Color getColor() {
        return color;
    }

    public void setColor(net.kyori.adventure.bossbar.BossBar.Color color) {
        this.color = color;
    }

    public net.kyori.adventure.bossbar.BossBar.Overlay getStyle() {
        return style;
    }

    public void setStyle(net.kyori.adventure.bossbar.BossBar.Overlay style) {
        this.style = style;
    }

    public List<net.kyori.adventure.bossbar.BossBar.Flag> getFlags() {
        return flags;
    }

    public void setFlags(List<net.kyori.adventure.bossbar.BossBar.Flag> flags) {
        this.flags = flags;
    }
}
