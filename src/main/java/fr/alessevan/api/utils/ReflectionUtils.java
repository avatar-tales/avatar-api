package fr.alessevan.api.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class for reflection
 */
public class ReflectionUtils {

    /**
     * Get all fields of an object
     *
     * @param t   the object
     * @param <T> the type of the object
     * @return the list of fields
     */
    public static <T> List<Field> getFields(T t) {
        List<Field> fields = new ArrayList<>();
        Class<?> clazz = t.getClass();
        while (clazz != Object.class) {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        }
        return fields;
    }

}
