package fr.alessevan.api.utils;

/**
 * Represents a pair of two objects
 *
 * @param <F> first type
 * @param <S> second type
 */
public class Pair<F, S> {

    /**
     * First object
     */
    private F first;
    /**
     * Second object
     */
    private S second;

    /**
     * Create a new pair
     *
     * @param first  first object
     * @param second second object
     */
    public Pair(F first, S second) {
        this.first = first;
        this.second = second;
    }

    /**
     * Create a new pair
     *
     * @param first  first object
     * @param second second object
     * @param <F>    first type
     * @param <S>    second type
     * @return the new pair
     */
    public static <F, S> Pair<F, S> of(F first, S second) {
        return new Pair<>(first, second);
    }

    /**
     * Get the first object
     *
     * @return the first object
     */
    public F getFirst() {
        return this.first;
    }

    /**
     * Set the first object
     *
     * @param first the first object
     */
    public void setFirst(F first) {
        this.first = first;
    }

    /**
     * Get the first object
     *
     * @return the first object
     */
    public F first() {
        return this.first;
    }

    /**
     * Get the second object
     *
     * @return the second object
     */
    public S getSecond() {
        return this.second;
    }

    /**
     * Set the second object
     *
     * @param second the second object
     */
    public void setSecond(S second) {
        this.second = second;
    }

    /**
     * Get the second object
     *
     * @return the second object
     */
    public S second() {
        return this.second;
    }

    /**
     * Set the first object
     *
     * @param first the first object
     */
    public void first(F first) {
        this.first = first;
    }

    /**
     * Set the second object
     *
     * @param second the second object
     */
    public void second(S second) {
        this.second = second;
    }

    /**
     * Swap the two objects
     *
     * @return the new pair
     */
    public Pair<S, F> swap() {
        return new Pair<>(this.second, this.first);
    }

    /**
     * Check if the pair is equal to another object
     *
     * @param o the other object
     * @return true if the pair is equal to the other object
     */
    @Override
    public boolean equals(Object o) {
        if (o == null || o.getClass() != getClass()) return false;
        if (o instanceof Pair<?, ?> obj) {
            return (obj.first == first && obj.second == second);
        }
        return false;
    }

    /**
     * Get the hash code of the pair
     *
     * @return the hash code of the pair
     */
    @Override
    public int hashCode() {
        return first.hashCode() ^ second.hashCode();
    }

}
