package fr.alessevan.api.utils;

import java.time.Duration;
import java.util.UUID;

public class DisplayBar {

    private final UUID id;
    private final String message;
    private boolean permanent;
    private Duration duration;
    private int priority;
    private boolean visible;

    public DisplayBar(String message) {
        this(message, false);
    }

    public DisplayBar(String message, boolean permanent) {
        this(message, permanent, 0);
    }

    public DisplayBar(String message, boolean permanent, int priority) {
        this(message, permanent, priority, true);
    }

    public DisplayBar(String message, boolean permanent, int priority, boolean visible) {
        this(message, permanent, priority, visible, Duration.ofSeconds(5));
    }

    public DisplayBar(String message, boolean permanent, int priority, boolean visible, Duration duration) {
        this(UUID.randomUUID(), message, permanent, priority, visible, duration);
    }


    public DisplayBar(UUID id, String message, boolean permanent, int priority, boolean visible, Duration duration) {
        this.id = id;
        this.message = message;
        this.permanent = permanent;
        this.priority = priority;
        this.visible = visible;
        this.duration = duration;
    }

    public UUID getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public boolean isPermanent() {
        return permanent;
    }

    public void setPermanent(boolean permanent) {
        this.permanent = permanent;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}
